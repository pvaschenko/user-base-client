Ext.define('UsersBaseApp.model.City', {
    extend: 'Ext.data.Model',
    fields: ['id','name']
});