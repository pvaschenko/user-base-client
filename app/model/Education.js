Ext.define('UsersBaseApp.model.Education', {
    extend: 'Ext.data.Model',
    fields: ['id','name']
});