Ext.define('UsersBaseApp.model.Users', {
    extend: 'Ext.data.Model',
    fields: ['id','name','q_id','qualification','city']
});