Ext.define('UsersBaseApp.store.CityStore', {
    extend: 'Ext.data.Store',
    model: 'UsersBaseApp.model.City',
    autoLoad: true,
    storeId: 'CityStore',
    proxy: {
        type: 'ajax',
        url: 'http://localhost:1112/api/city',
        reader: {
            type: 'json',
            rootProperty: 'data',
        }
    }
});