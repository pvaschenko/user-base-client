Ext.define('UsersBaseApp.store.UsersStore', {
    extend: 'Ext.data.Store',
    model: 'UsersBaseApp.model.Users',
    autoLoad: true,
    storeId: 'UsersStore',
    proxy: {
        type: 'ajax',
        url: 'http://localhost:1112/api/index',
        reader: {
            type: 'json',
            rootProperty: 'data',
        }
    }
});