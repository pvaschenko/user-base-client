Ext.define('UsersBaseApp.store.EducationStore', {
    extend: 'Ext.data.Store',
    model: 'UsersBaseApp.model.Education',
    autoLoad: true,
    storeId: 'EducationStore',
    proxy: {
        type: 'ajax',
        url: 'http://localhost:1112/api/education',
        reader: {
            type: 'json',
            rootProperty: 'data',
        }
    }
});