Ext.define('UsersBaseApp.controller.Users', {
    extend: 'Ext.app.Controller',
    views: ['UsersList', 'EducationFilter', 'CityFilter', 'User'],
    stores: ['UsersStore', 'EducationStore', 'CityStore'],
    models: ['Users', 'Education', 'City'],
    init: function() {
        this.control({
            'userslist': {
                itemdblclick: this.editUser
            },
            'userwindow button[action=new]': {
                click: this.createUser
            },
            'userwindow button[action=save]': {
                click: this.updateUser
            },
            'userwindow button[action=delete]': {
                click: this.deleteUser
            },
            'userwindow button[action=clear]': {
                click: this.clearForm
            }
        });
    },
    // обновление
    updateUser: function(button) {
        var win    = button.up('window'),
            form   = win.down('form'),
            values = form.getValues(),
            id = form.getRecord().get('id');
            values.id=id;
			cities = values.city;
			values.city = cities.join(',');
        Ext.Ajax.request({
            url: 'http://localhost:1112/api/update/'+id,
            params: values,
            success: function(response){
                var data=Ext.decode(response.responseText);
                if(data.data == 'ok'){
                    var store = Ext.widget('userslist').getStore();
                    store.load();
                    Ext.Msg.alert('Обновление',data.data);
                }
                else{
                    Ext.Msg.alert('Обновление','Не удалось обновить пользователя');
                }
            }
        });
    },
    // создание
    createUser: function(button) {
        var win    = button.up('window'),
            form   = win.down('form'),
            values = form.getValues();
			cities = values.city;
			values.city = cities.join(',');
        Ext.Ajax.request({
            url: 'http://localhost:1112/api/create',
            params: values,
            success: function(response, options){
                var data=Ext.decode(response.responseText);
                if(data.data == 'ok'){
                    Ext.Msg.alert('Создание',data.data);
                    var store = Ext.widget('userslist').getStore();
                    store.load();
                }
                else{
                    Ext.Msg.alert('Создание','Не удалось добавить пользователя');
                }
            }
        });
    },
    // удаление
    deleteUser: function(button) {
        var win    = button.up('window'),
            form   = win.down('form'),
            id = form.getRecord().get('id');
        Ext.Ajax.request({
            url: 'http://localhost:1112/api/delete/'+id,
            params: {id:id},
            success: function(response){
                var data=Ext.decode(response.responseText);
                if(data.data == 'ok'){
                    Ext.Msg.alert('Удаление',data.data);
                    var store = Ext.widget('userslist').getStore();
                    var record = store.getById(id);
                    store.remove(record);
                    form.getForm.reset();
                }
                else{
                    Ext.Msg.alert('Удаление','Не удалось удалить пользователя');
                }
            }
        });
    },
    clearForm: function(grid, record) {
        var view = Ext.widget('userwindow');
        view.down('form').getForm().reset();
    },
    editUser: function(grid, record) {
        var view = Ext.widget('userwindow');
        view.down('form').loadRecord(record);
    }
});