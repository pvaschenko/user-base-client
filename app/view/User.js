Ext.define('Education', {
			extend: 'Ext.data.Model',
			fields: ['id','name']
});
var store1 = Ext.create('Ext.data.Store', {
	model: 'Education',
	proxy: {
		type: 'ajax',
		url: 'http://localhost:1112/api/education',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
		fields: ['id','name'],
	}
});
Ext.define('City', {
	extend: 'Ext.data.Model',
	fields: ['id','name']
});
var store2 = Ext.create('Ext.data.Store', {
	model: 'City',
	proxy: {
		type: 'ajax',
		url: 'http://localhost:1112/api/city',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
		fields: ['id','name'],
	},
	autoLoad: true
});
Ext.define('UsersBaseApp.view.User', {
    extend: 'Ext.window.Window',
    alias: 'widget.userwindow',
    title: 'Пользователь',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [{
                xtype: 'form',
                items: [{
                        xtype: 'textfield',
                        name : 'name',
                        fieldLabel: 'ФИО'
                    },{
                        xtype: 'combobox',
                        name : 'qualification',
						store: store1,
                        fieldLabel: 'Образование',
						valueField:'id',
						displayField:'name',
						queryMode:'remote'						
                    },{
                        xtype: 'combobox',
                        name : 'city',
						store: store2,
                        fieldLabel: 'Город',
						valueField:'id',
						displayField:'name',
						queryMode:'remote',
						multiSelect: true,
						listeners: {
						  change: function(combo, eOpts){
						   var store = combo.getStore();
						   var values = [];
						   Ext.Array.each(combo.value, function(id){
							   values.push(store.getById(id+''));
						   });
						  combo.setValue(values);
						 }
					    }
                }]
            }];
        this.dockedItems=[{
            xtype:'toolbar',
            docked: 'top',
            items: [{
                text:'Создать',
                iconCls:'new-icon',
                action: 'new'
            },{
                text:'Сохранить',
                iconCls:'save-icon',
                action: 'save'
            },{
                text:'Удалить',
                iconCls:'delete-icon',
                action: 'delete'
            }]
        }];
        this.buttons = [{
                text: 'Очистить',
                scope: this,
                action: 'clear'
        }];
 
        this.callParent(arguments);
    }
});