Ext.define('UsersBaseApp.view.EducationFilter' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.educationfilter',
    multiSelect: true,
    title: 'Образование',
    store: 'EducationStore'
});