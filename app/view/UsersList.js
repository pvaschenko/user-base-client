Ext.define('UsersBaseApp.view.UsersList' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.userslist',
 
    title: 'Пользователи',
    store: 'UsersStore',
     
    initComponent: function() {
        this.columns = [
            {header: 'ФИО',  dataIndex: 'name',  flex: 1},
            {header: 'Образование',  dataIndex: 'qualification',  flex: 1},
            {header: 'Город', dataIndex: 'city', flex: 1}
        ];
         
        this.callParent(arguments);
    }
});