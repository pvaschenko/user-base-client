Ext.define('UsersBaseApp.view.CityFilter' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.cityfilter',
    multiSelect: true,
    title: 'Город',
    store: 'CityStore',
     
    initComponent: function() {
        this.columns = [
            {header: 'Город',  dataIndex: 'name',  flex: 1},
        ];
         
        this.callParent(arguments);
    }
});