Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '../extjs/packages/ux/classic/src/');
var education = null;
var city = null;
Ext.application({
    requires: ['Ext.container.Viewport','Ext.form.Panel','Ext.ux.form.MultiSelect'],
    name: 'UsersBaseApp',
    appFolder: 'app',
    controllers: ['Users'],
     
    launch: function() {
		Ext.create('Ext.form.Panel', {
			title: 'Фильтр по образованию',
			width: 220,
			renderTo: document.body,
			items:[{
            anchor: '100%',
            xtype: 'multiselect',
            msgTarget: 'side',
            fieldLabel: 'Образование',
            name: 'educationfilter',
            id: 'educationfilter-field',
            allowBlank: false,
            store: {
                fields: [ 'id', 'name' ],
                proxy: {
				type: 'ajax',
					url: 'http://localhost:1112/api/education',
					reader: {
						type: 'json',
						rootProperty: 'data',
					}
                },
                autoLoad: true
            },
            valueField: 'id',
            displayField: 'name',
            ddReorder: true,
			listeners: {
			  change: function(combo, eOpts){
			   var store = combo.getStore();
			   var values = [];
			   Ext.Array.each(combo.value, function(id){
				   values.push(store.getById(id+''));
			   });
			   combo.setValue(values);
			   var store = Ext.widget('userslist').getStore();
			   education = values.join(',');
			   store.load({
				   params: {city: city, education: education}
			   });
			 }
			}
        }]
		});
		Ext.create('Ext.form.Panel', {
			xtype: 'cityfilter',
			title: 'Фильтр по городу',
			width: 220,
			renderTo: document.body,
			items:[{
            anchor: '100%',
            xtype: 'multiselect',
            msgTarget: 'side',
            fieldLabel: 'Город',
            name: 'cityfilter',
            id: 'cityfilter-field',
            allowBlank: false,
            store: {
                fields: [ 'id', 'name' ],
                proxy: {
				type: 'ajax',
					url: 'http://localhost:1112/api/city',
					reader: {
						type: 'json',
						rootProperty: 'data',
					}
                },
                autoLoad: true
            },
            valueField: 'id',
            displayField: 'name',
            ddReorder: true,
			listeners: {
			  change: function(combo, eOpts){
			   var store = combo.getStore();
			   var values = [];
			   Ext.Array.each(combo.value, function(id){
				   values.push(store.getById(id+''));
			   });
			   combo.setValue(values);
			   var store = Ext.widget('userslist').getStore();
			   city = values.join(',');
			   store.load({
				   params: {city: city, education: education}
			   });
			 }
			}
        }]
		});
		Ext.create({
			xtype: 'userslist',
			title: 'Пользователи',
			width: '100%',
			renderTo: document.body
		});
	}
});